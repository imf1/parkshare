const { Board, Proximity } = require("johnny-five");
const request = require('request-promise');
const config = require('./config');
const board = new Board();

var options = {
    
    method: 'POST',
    uri: config.parkshareRoot + "/sensors/parking/car_detected",
    body: {
        apisKey: config.sensorKey
    },
    json: true
};

board.on("ready", () => {
  const proximity = new Proximity({
    controller: "HCSR04",
    pin: 7
  });

  proximity.on("change", () => {
    const {centimeters} = proximity;
    if(centimeters < config.maxDistance)
    {
        request(options)
            .then(function (json) {
                if(json.success == false)
                {
                    console.log(json.message != undefined ? json.message : json.error);
                }
            })
            .catch(function (err) {
                console.log(err.message);
            });
    }
  });
});
