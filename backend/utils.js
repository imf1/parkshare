const bcrypt = require('bcrypt')
const crypto = require("crypto");
const Promise = require("bluebird");
const { User, ParkingSpace, Car, Client, Token, Rent, ParkingSpaceRent, CarReservation, Reservation } = require('./db_models');
const sequelize = require('./db');

const saltRounds = 10;


function generateToken(data) {
    var random = Math.floor(Math.random() * 1610612741);
    var timestamp = (new Date()).getTime();
    var sha256 = crypto.createHmac("sha256", random + "SOMETHING" + timestamp);

    return {
        token: sha256.update(data).digest("hex"),
        expires: Math.round(timestamp / 1000) + 7 * 24 * 60 * 60
    };
}

function checkBody(body, params) {
    for (var index = 0; index < params.length; ++index)
        if (body[params[index]] == undefined)
            return false;
    return true;
}

module.exports = {
    registerUser: (userData) => {
        if (!checkBody(userData, ["lastName", "firstName", "username", "email", "password"])) {
            return Promise.resolve({ success: false, message: 'A required parameter was missing' });
        };
        return User.count({ where: { username: userData.username,  email: userData.email} })
            .then(count => {
                if (count == 0) {
                    var salt = bcrypt.genSaltSync(saltRounds);
                    var hash = bcrypt.hashSync(userData.password, salt);
                    return User.create({
                        lastName: userData.lastName,
                        firstName: userData.firstName,
                        username: userData.username,
                        email: userData.email,
                        password: hash
                    }).then(result => {
                        return { success: true };
                    }).catch(err => {
                        return { success: false, error: err.message };
                    });
                }
                else {
                    return { success: false, message: "There already exists an user with that email." };
                }
            });
    },
    getUser: (username) => {
        return User.findOne({
            where: {
                username: username
            }
        }).then(result => {
            return result;
        }).catch(err => {
            return null;
        });
    },
    addCar: (user, car) => {
        if (!checkBody(car, ["plateNumber"])) {
            return Promise.resolve({ success: false, message: 'A required parameter was missing' });
        }
        return Car.create({
            plateNumber: car.plateNumber,
            userId: user.id
        }).then(result => {
            return { success: true, message: "The car was added successfully." };
        }).catch(err => {
            return { success: false, error: err.message }
        });
    },
    addParkingSpace: (user, parkingSpaceData) => {
        if (!checkBody(parkingSpaceData, ["location"])) {
            return Promise.resolve({ success: false, message: 'A required parameter was missing' });
        }
        return ParkingSpace.create({
            location: parkingSpaceData.location,
            apiKey: generateToken(user.username + ":" + parkingSpaceData.location).token,
            userId: user.id
        }).then(result => {
            return { success: true, message: "The parking space was added successfully." };
        }).catch(err => {
            return { success: false, error: err.message }
        })
    },
    validateClient: (credentials, req, cb) => {
        return Client.count({ where: { id: credentials.clientId, secret: credentials.clientSecret } }).then(count => {
            cb(null, count > 0);
        }).catch(err => {
            cb(err, false);
        });
    },
    grantUserToken: (credentials, req, cb) => {
        return User.findOne({ where: { username: credentials.username } }
        ).then(user => {
            if (user == null)
                cb(null, false);
            else if (bcrypt.compareSync(credentials.password, user.password)) {
                var tokenResult = generateToken(user.username);
                return Token.create({
                    accessToken: tokenResult.token,
                    accessTokenExpireTime: tokenResult.expires,
                    userId: user.id
                }).then(token => {
                    cb(null, token.accessToken);
                });
            }
        }).catch(err => {
            cb(err, false);
        });
    },
    authenticateToken: (token, req, cb) => {
        return Token.findOne(
            {
                where: { accessToken: token },
                include: [
                    User
                ]
            }).then(token => {
                if (token == null)
                    cb(null, false);
                else {
                    req.username = token.user.username;
                    cb(null, true);
                }
            }).catch(err => {
                cb(err, false);
            });
    },
    getParkingSpaceBySensorKey: (sensorKey) => {
        if (sensorKey == undefined) {
            return Promise.resolve({ success: false, error: 'Sensor key was undefined' });
        }
        return ParkingSpace.findOne({
            where: {
                apiKey: sensorKey
            },
            include: [
                User
            ]
        }).then(data => {
            if (data == null)
                return { success: false, error: 'Parking spot was not found.' };
            return { success: true, data };
        }).catch(err => {
            return { success: false, error: err.message };
        });
    },
    addRent: (user, rentData, parkingSpace) => {
        if (!checkBody(rentData, ["startDate", "endDate", "pricePerHour"])) {
            return Promise.resolve({ success: false, message: 'A required parameter was missing' });
        }
        return Rent.create({
            startDate: rentData.startDate,
            endDate: rentData.endDate,
            pricePerHour: rentData.pricePerHour,
        }).then(result => {
            return ParkingSpaceRent.create({
                parkingspaceId: parkingSpace.id,
                rentId: result.id
            });
        }).then(result => {
            return { success: true, message: "The rent was added." };
        }).catch(err => {
            return { success: false, error: err.message };
        });
    },
    checkIfParkingBelongsToUser: (user, parkingSpaceId) => {
        return ParkingSpace.findOne({
            where: {
                id: parkingSpaceId
            },
            include: [
                User
            ]
        }).then(data => {
            if (data == null)
                return { success: false, message: 'Parking spot was not found.' };
            return { success: true, data };
        }).catch(err => {
            return { success: false, error: err.message };
        });
    },
    checkIfReservationCanBeMade: (rentId, reservationData) => {
        if (!checkBody(reservationData, ["startDate", "endDate"])) {
            return Promise.resolve({ success: false, message: 'A required parameter was missing' });
        }

        const reservationStartDate = new Date(reservationData.startDate);
        console.log(reservationStartDate);
        const reservationEndDate = new Date(reservationData.endDate);
        console.log(reservationEndDate);

        return Rent.count({
            where: {
                id: rentId
            }
        }).then(rentResult => {
            if (rentResult == 0)
            {
                return Promise.resolve({ success: false, message: 'The desired rent does not exist.' });
            }
            return Reservation.count({
                where: {
                    startDate: { $not: { $lt: reservationEndDate } },
                    endDate: { $not: { $gt: reservationStartDate } },
                }
            }).then(result => {
                console.log(result);
                if (result > 0)
                    return { success: true, message: 'Reservations can be added.' };
                return { success: false, message: 'Reservations can\'t be added at that interval of hours.' }
            })
        }).catch(err => {
            return { success: false, error: err.message };
        });
    },
    addReservation: (user, reservationData, carId, rentId) =>{
        if (!checkBody(reservationData, ["startDate", "endDate"]) || !carId || !user) {
            return Promise.resolve({ success: false, message: 'A required parameter was missing' });
        }

        const reservationStartDate = new Date(reservationData.startDate);
        const reservationEndDate = new Date(reservationData.endDate);
        console.log(reservationEndDate);
        return Rent.findOne({
            attributes: ['pricePerHour'],
            where: { id: rentId}
        }).then(result => {
            if(result == null)
                return {success: false, message: 'The price per hour was not found'};
            
                console.log('Price per hour: ' + result.pricePerHour);
                 difference = (reservationEndDate.getHours() - reservationStartDate.getHours());
                console.log(result.pricePerHour);
                const totalPrice = result.pricePerHour * difference;
                console.log('The total price is: ' + totalPrice);
                return Reservation.create({
                    startDate: reservationStartDate,
                    endDate: reservationEndDate,
                    totalPrice: totalPrice
                }).then(reservation => {
                    return CarReservation.create({
                        carId: carId,
                        reservationId: reservation.id
                    }).then(carReservation =>{
                        return {success: true, data: reservation, message:'The reservation was added.'};
                    });
                });
        }).catch(err => {
            return {success: false, error: err.message};
        });
    },
    getAllRents : () => {
        return Rent.findAll({
            include: [ParkingSpace]
        }).then(result => {return {success: true, data: result}}).catch(err => {return {success: false, error: err.message}});
    },
    getUserCars: (user) => {
        return Car.findAll({
            where: {
                userId: user.id
            }
        }).then(result => {return {success: true, data: result}}).catch(err => {return {success: false, error: err.message}});
    }
};

//add a rent and don't forget to add it to rent_parkspace also
// rents/add
//check for available rents and return