module.exports = (server, utils) => {
    var { requireAuth, requireBody, ownsParkingSpace, canReservationBeMade } = require("./middleware")(utils);

    server.get("/", (req, res, next) => {
        res.json({
            message: "Welcome to Park Share API."
        });
        next();
    });
    
    server.post("/register", (req, res, next) => {
        utils.registerUser(req.body)
            .then(result => {
                res.json(result);
            });
    });

    server.post("/cars/add", requireAuth, requireBody, (req, res, next) =>{
        const plateNumber = req.body.plateNumber;
        utils.addCar(req.user, {plateNumber}).then((obj) => {
            res.json(obj);
        });
    });

    server.post("/sensors/parking/car_detected", requireBody, (req, res, next)=>{
        const apiKey = req.body.apiKey;
        utils.getParkingSpaceBySensorKey(apiKey).then(result=>{
            if(result.success == true){
                console.log(`${result.data.user.username}: ALERT! Someone tried to enter parking space #${result.data.id} at ${result.parkingSpace.location}.`);
                res.json({success: true});
            } else{
                res.json(result);
            }
        });
    });

    server.post("/rents/add", requireAuth, requireBody, ownsParkingSpace, (req, res, next) => {
        const parkingSpaceId = req.body.parkingSpaceId;
        const startDate = req.body.startDate;
        const endDate = req.body.endDate;
        const pricePerHour = req.body.pricePerHour;
        utils.addRent(req.user, {parkingSpaceId, startDate, endDate, pricePerHour}, req.parkingSpace).then(result =>{
            res.json(result);
        });        
    });

    server.post("/parkingspaces/add", requireAuth, requireBody, (req, res, next) => {
        const location = req.body.location;
        utils.addParkingSpace(req.user, {location}).then(result => {
            res.json(result);
        });
    })

    server.post("/reservations/add", requireAuth, requireBody, (req, res, next) => {
         utils.addReservation(req.user, {startDate: req.body.startDate, endDate: req.body.endDate}, req.body.carId, req.body.rentId).then(result => {
             res.json(result);
         });
    });

    server.post("/token/validate", requireAuth, (req, res, next) => {
        res.json({success: true});
    })

    server.get("/rents/all", requireAuth, (req, res, next) => {
        utils.getAllRents().then(result => {res.json(result)});
    });

    server.get("/cars/user/get", requireAuth, (req, res, next) => {
        utils.getUserCars(req.user).then(result => {res.json(result)});
    });
};