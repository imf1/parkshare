const db = require('./db');
db.authenticate()

const User = require('./models/User')(db);
const Reservation = require('./models/Reservation')(db);
const ParkingSpace = require('./models/ParkingSpace')(db);
const Rent = require('./models/Rent')(db);
const Car = require('./models/Car')(db);
const Client = require('./models/Client')(db);
const Token = require('./models/Token')(db);
const CarReservation = require('./models/CarReservation')(db);
const ParkingSpaceRent = require('./models/ParkingSpaceRent')(db);

User.hasOne(Car);

Token.belongsTo(User);

Car.belongsToMany(Reservation, { through: CarReservation });
Reservation.belongsToMany(Car, { through: CarReservation });

//Reservation.hasOne(Rent);
Reservation.belongsTo(Rent);

ParkingSpace.belongsToMany(Rent, { through: ParkingSpaceRent });
Rent.belongsToMany(ParkingSpace, { through: ParkingSpaceRent });

ParkingSpace.belongsTo(User);

Reservation.belongsTo(User, {
    as: 'owner',
    foreignKey: 'owner_id',
});

Reservation.belongsTo(User, {
    as: 'guest',
    foreignKey: 'guest_id'
});

db.sync();

module.exports = {
    User, Reservation, ParkingSpace, Rent, Car, Client, Token, ParkingSpaceRent, CarReservation
};