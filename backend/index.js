var restify = require("restify");
var restifyOAuth2 = require("restify-oauth2");

var utils = require("./utils");
var addRoutes = require("./routes");

const cors = require("restify-cors-middleware")({  
    origins: ["*"],
    allowHeaders: ["Authorization"],
    exposeHeaders: ["Authorization"]
});

var server = restify.createServer({ name: "Park Share", version: "1.0.0" });
server.pre(cors.preflight);
server.use(restify.plugins.authorizationParser());
server.use(restify.plugins.bodyParser({ mapParams: false }));
server.use(cors.actual);

var hooks = {
    validateClient: utils.validateClient,
    grantUserToken: utils.grantUserToken,
    authenticateToken: utils.authenticateToken
};
restifyOAuth2.ropc(server, { tokenEndpoint: '/login', hooks: hooks });

addRoutes(server, utils);

server.listen(3465, function () {
    console.log('%s listening at %s', server.name, server.url);
});