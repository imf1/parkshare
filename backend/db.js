const config = require("./config.json");
const dbInfo = config.db;

const Sequelize = require('sequelize');
const sequelize = new Sequelize(dbInfo.DB, dbInfo.USERNAME, dbInfo.PASSWORD, {
    host: dbInfo.HOST,
    dialect: dbInfo.DIALECT,
    dialectOptions: {
        timezone: dbInfo.TIMEZONE
    },
    logging: false
});

module.exports = sequelize;