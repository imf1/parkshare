const Sequelize = require('sequelize');
const Model = Sequelize.Model;

class Car extends Model {}
module.exports = (sequelize) =>{
    Car.init({
      plateNumber: {
        type: Sequelize.STRING,
        allowNull: false
      }
    },{
      sequelize,
      modelName: 'car'
    });
    return Car;
}

