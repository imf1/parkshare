const Sequelize = require('sequelize');
const Model = Sequelize.Model;

class Client extends Model { }
module.exports = (sequelize) => {
    Client.init({
       grants:{
           type: Sequelize.STRING,
           allowNull: false
       },
       secret:{
           type: Sequelize.STRING,
           allowNull: false
       }
    }, {
        sequelize,
        modelName: 'client'
    });
    return Client;
};