const Sequelize = require('sequelize');
const Model = Sequelize.Model;

class Token extends Model { }
module.exports = (sequelize) => {
    Token.init({
       accessToken: {
           type: Sequelize.STRING,
           allowNull: false
       },
       accessTokenExpireTime: {
           type: Sequelize.DATE,
           allowNull: false
       }
    }, {
        sequelize,
        modelName: 'token'
    });
    return Token;
};