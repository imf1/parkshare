const Sequelize = require('sequelize');
const Model = Sequelize.Model;

class Reservation extends Model {}
module.exports = (sequelize) =>{
    Reservation.init({
      startDate: {
          type: Sequelize.DATE,
          allowNull: false
      },
      endDate: {
        type: Sequelize.DATE,
        allowNull: false
      },
      totalPrice: {
        type: Sequelize.FLOAT,
        allowNull: true
      }
    },{
      sequelize,
      modelName: 'reservation'
    });
    return Reservation;
}