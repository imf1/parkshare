const Sequelize = require('sequelize');
const Model = Sequelize.Model;

class ParkingSpace extends Model {}
module.exports = (sequelize) =>{
    ParkingSpace.init({
      location: {
        type: Sequelize.STRING,
        allowNull: false
      },
      apiKey:{
        type: Sequelize.STRING,
        allowNull: false
      }
    },{
      sequelize,
      modelName: 'parkingspace'
    });
    return ParkingSpace;
}