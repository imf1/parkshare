const Sequelize = require('sequelize');
const Model = Sequelize.Model;

class Rent extends Model {}
module.exports = (sequelize) =>{
    Rent.init({
      startDate: {
        type: Sequelize.DATE,
        allowNull: false
      },
      endDate: {
        type: Sequelize.DATE,
        allowNull: false
      },
      pricePerHour: {
        type: Sequelize.FLOAT,
        allowNull: false,
        validate: { min: 0.01, max: 99.99 }
      }
    },{
      sequelize,
      modelName: 'rent'
    });
    return Rent;
}

// plateNumber:{
//   type: Sequelize.STRING,
//   allowNull: false
// }