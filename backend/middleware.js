const errors = require("restify-errors");
module.exports = (utils) =>{
    return {
        requireAuth: (req, res, next) => {
            if (!req.username) {
                res.send(new errors.UnauthorizedError("Not allowed."));
                next(false);
            }
            else {
                utils.getUser(req.username)
                .then(user => {
                    if(user == null)
                    {
                        res.send(new errors.UnauthorizedError("Not allowed."));
                        next(false);
                    }
                    else
                    {
                        req.user = user;
                        next();
                    }
                });
            }
        },
        requireBody: (req, res, next) => {
            if(req.body == undefined)
            {
                res.json({success: false});
                next(false);
            }
            else
            {
                next();
            }
        },
        ownsParkingSpace: (req, res, next) =>{
            if(req.body == undefined)
            {
                res.json({success: false});
                next(false);
            }
            else
            {
                if(!req.body.parkingSpaceId){
                    res.json({success:false});
                    next(false);
                }
                else
                {
                    utils.checkIfParkingBelongsToUser(req.user, req.body.parkingSpaceId).then(result =>{
                        if(result.success == true)
                        {
                            console.log(result.data);
                            req.parkingSpace = result.data;
                            next();   
                        } 
                        else
                        {
                            res.json({success:false});
                            next(false);
                        }
                    });
                }
            }
        },
        canReservationBeMade: (req, res, next) => {
            if(req.body == undefined)
            {
                res.json({success: false});
                next(false);
            }
            else
            {
                if(!req.body.rentId || !req.body.startDate || !req.body.endDate)
                {
                    res.json({success: false, message: 'A parameter is missing.'});
                    next(false);
                }

                utils.checkIfReservationCanBeMade(req.body.rentId, {startDate: req.body.startDate, endDate: req.body.endDate}).then(result=> {
                    if(result.success == true)
                        next();
                    else 
                    {
                        res.json({success: false, message: 'Reservations can\'t be made.'});
                        next(false);
                    }
                })
            }
        }
    };
};