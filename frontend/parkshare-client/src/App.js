import * as React from "react";
import * as ReactDOM from "react-dom";
import * as ReactRouter from 'react-router';
import { BrowserRouter } from "react-router-dom";

import Main from "./Main";
import Login from "./Login";
import Register from "./Register";
import Dashboard from "./Dashboard";

let Route = ReactRouter.Route;

export default props => ReactDOM.render(
    <BrowserRouter>
        <Route exact path="/" component={Main} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/dashboard" component={Dashboard} />
    </BrowserRouter>,
    document.getElementById("root")
);