import React, { Component } from 'react';
import './Login.css';
import 'bootstrap/dist/css/bootstrap.css';
import { Form, Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import Cookies from 'universal-cookie';
import { ToastContainer } from "react-toastr";
const cookies = new Cookies();
const config = require("./config");
const { validateToken } = require("./lib");

class Login extends Component {
  constructor(props) {
    super(props);
    this.clickButton = this.clickButton.bind(this);
    this.render = this.render.bind(this);
    this.data = new FormData();
  }

  clickButton(event) {
    if(!this.form.reportValidity()) return;
    this.data.set("grant_type", "password");
    fetch(config.apiRoot + "/login", {
      method: 'POST',
      body: this.data,
      headers: new Headers({
        'Authorization': 'Basic '+btoa(config.clientCredentials.id + ':' +config.clientCredentials.secret), 
      })
    })
    .then(result => {
      console.log(result.statusCode);
      return result.json();
    })
    .then(data => {
      if(data.access_token)
      {
        cookies.set('accessToken', data.access_token, {path: '/'});
        this.props.history.push('/dashboard');
      } else {
        this.container.error(
          <strong>Error!</strong>,
          <em>Invalid credentials.</em>
        );
      }
    });
  }

  render() {
    validateToken(cookies.get('accessToken'))
    .then(result => {
      return result.json();
    }).then(data => {
      if(data.success)
        this.props.history.push('/dashboard');
    })
    return (
      <><ToastContainer ref={ref => this.container = ref} className="toast-top-right"/>
      <div className="form-wrapper">
        <Form className="form-login" ref={form => this.form = form}>
          <h1 className="header-title">Login</h1>
          <Form.Group >
            <Form.Label>Username</Form.Label>
            <Form.Control type="text" placeholder="Enter username" onChange={(event) => { this.data.set("username", event.target.value); }} />
          </Form.Group>

          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" onChange={(event) => { this.data.set("password", event.target.value); }} />
          </Form.Group>
          <Button variant="primary" type="button" onClick={this.clickButton} size="lg" block>
            Submit
          </Button>{" "}
        </Form>
      </div></>
    );
  }
}

export default Login;
