import React, { createRef, Component } from 'react'
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import { Form, Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import './Dashboard.css';
import 'leaflet/dist/leaflet.css'
import Cookies from 'universal-cookie';
import { ToastContainer } from "react-toastr";
const cookies = new Cookies();
const config = require("./config");
const { validateToken } = require("./lib");

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.render = this.render.bind(this);
    this.checkLoggedIn = this.checkLoggedIn.bind(this);
    this.clickButton = this.clickButton.bind(this);
    this.data = new FormData();
  }

  state = {
    rents: [],
    cars: []
  }

  clickButton() {
    if(!this.form.reportValidity()) return;
    console.log(this.data);
    fetch(config.apiRoot + "/reservations/add", {
      method: 'POST',
      body: this.data,
      headers: new Headers({
        'Authorization': 'Bearer ' + cookies.get('accessToken'),
      }),
      cors: "no-cors"
    })
    .then(result => {
      return result.json();
    })
    .then(data => {
      this.container.info(data.message);
    });
  }

  checkLoggedIn() {
    validateToken(cookies.get('accessToken'))
      .then(result => {
        return result.json();
      }).then(data => {
        if (!data.success)
          this.props.history.push('/login');
      })
  }

  componentDidMount() {
    fetch(config.apiRoot + "/rents/all", {
      method: 'GET',
      headers: new Headers({
        'Authorization': 'Bearer ' + cookies.get('accessToken'),
      }),
      cors: "no-cors"
    }).then(result => {
      return result.json();
    }).then(data => {
      var selectOptions = data.data.map((option, index) => { return { display: option.parkingspaces[0].location, value: option.id } });
      this.setState({ rents: [{ value: '', display: '(Select)' }].concat(selectOptions) });
    }).catch(err => {
      console.log(err);
    })

    
    fetch(config.apiRoot + "/cars/user/get", {
      method: 'GET',
      headers: new Headers({ 
        'Authorization': 'Bearer ' + cookies.get('accessToken'),
      }),
      cors: "no-cors"
    }).then(result => {
      return result.json();
    }).then(data => {
      console.log(data);
      var selectOptions = data.data.map((option, index) => { return { display: option.plateNumber, value: option.id } });
      this.setState({ cars: [{ value: '', display: '(Select)' }].concat(selectOptions) });
    }).catch(err => {
      console.log(err);
    })
  }

  render() {
    this.checkLoggedIn();
    return (
      <>
      <ToastContainer ref={ref => this.container = ref} className="toast-top-right"/>
      <Form className="form-login" ref={form => this.form = form}>
        <p>Rent:</p>
        <select class="form-control" onChange={(event) => { this.data.set("rentId", event.target.value); }}>
          {this.state.rents.map((option) => <option key={option.value} value={option.value}>{option.display}</option>)}
        </select>

        <p>Car:</p>
        <select class="form-control" onChange={(event) => { this.data.set("carId", event.target.value); }}>
          {this.state.cars.map((option) => <option key={option.value} value={option.value}>{option.display}</option>)}
        </select>

        <p>Start date:</p>
        <input type="datetime-local" onChange={(event) => { this.data.set("startDate", event.target.value); }} />

        <p>End date:</p>
        <input type="datetime-local" onChange={(event) => { this.data.set("endDate", event.target.value); }} />

        <Button variant="primary" type="button" onClick={this.clickButton} size="lg" block>
            Add reservation
        </Button>
      </Form>
      </>
    )
  }
};

export default Dashboard;