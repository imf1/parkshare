import React from "react";
import { Button } from "react-bootstrap";
import { Link } from 'react-router-dom';


import './App.css';
export default props => (
  <>
    
    <div  className="buttons-wrapper">
    <Link to="/login">
      <Button variant="primary" size="lg" block>
        Login
      </Button>{" "}
    </Link>
    <Link to="/register">
      <Button variant="primary" size="lg" block>
        Register
      </Button>{" "}
    </Link>
    </div>
  </>
);

