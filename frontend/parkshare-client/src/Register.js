import React, { Component } from 'react';
import './Register.css';
import './toastr.css';
import 'bootstrap/dist/css/bootstrap.css';
import { Form, Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
const config = require("./config");
import { ToastContainer } from "react-toastr";

class Register extends Component {
  constructor(props) {
    super(props);
    this.clickButton = this.clickButton.bind(this);
    this.render = this.render.bind(this);
    this.data = new FormData();
  }
  clickButton(event) {
    if(!this.form.reportValidity()) return;
    fetch(config.apiRoot + "/register", {
      method: 'POST',
      body: this.data
    })
    .then(result => {
      return result.json();
    })
    .then(data => {
      if(data.success == true)
        this.props.history.push('/login');
      else
        this.container.error(data.message);
    });
  }
  render() {
    return (
    <><ToastContainer ref={ref => this.container = ref} className="toast-top-right"/>
      <div className="form-wrapper">
        <Form className="form-login" ref={form => this.form = form}>
         <h1 className="header-title">Register</h1>
          <Form.Group>
            <Form.Label>Username</Form.Label>
            <Form.Control required type="text" placeholder="Enter username" onChange={(event) => { this.data.set("username", event.target.value); }} />
          </Form.Group>
          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control required type="password" placeholder="Password" onChange={(event) => { this.data.set("password", event.target.value); }}/>
          </Form.Group>
          <Form.Group>
            <Form.Label>Email</Form.Label>
            <Form.Control required type="email" placeholder="Email" onChange={(event) => { this.data.set("email", event.target.value); }} />
          </Form.Group>
          <Form.Group>
            <Form.Label>First name</Form.Label>
            <Form.Control required type="text" placeholder="First name" onChange={(event) => { this.data.set("firstName", event.target.value); }} />
          </Form.Group>
          <Form.Group>
            <Form.Label>Last name</Form.Label>
            <Form.Control required type="text" placeholder="Last name" onChange={(event) => { this.data.set("lastName", event.target.value); }} />
          </Form.Group>
          <Button variant="primary" type="button" onClick={this.clickButton}>
            Submit
      </Button>
        </Form>
      </div></>
    );
  }
}

export default Register;
