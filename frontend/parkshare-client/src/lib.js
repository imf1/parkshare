var config = require("./config");
module.exports = {
    validateToken: (token) => {
        return fetch(config.apiRoot + "/token/validate", {
          method: 'POST',
          headers: new Headers({
            'Authorization': 'Bearer '+ token, 
          }),
          cors: "no-cors"
        });
    }
};